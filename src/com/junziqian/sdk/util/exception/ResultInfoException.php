<?php
namespace com\junziqian\sdk\util\exception;
/**
 * 异常信息类
 * Class ResultInfoException
 * @package com\junziqian\sdk\util\exception
 * @edit yfx 2019-10-29
 */
class ResultInfoException extends \RuntimeException {
    /**
     * @var string 异常码
     */
    private $resultCode;

    /**
     * ResultInfoException constructor.
     * @param string $message 异常信息
     * @param string $resultCode 异常码
     */
    public function __construct($message = "",$resultCode="PARAM_ERROR"){
        parent::__construct($message, null, null);
    }

    /**
     * @return string 异常吗
     */
    public function getResultCode(){
        return $this->resultCode;
    }
}