<?php


namespace com\junziqian\sdk\util;

use com\junziqian\sdk\util\exception\ResultInfoException;
use com\junziqian\sdk\util\http\HttpClientUtils;
/**
 * Class RequestUtils http请求
 * @package com\junziqian\sdk\bean
 * @edit yfx 2019-10-29
 */
class RequestUtils{
    /**请求地址*/
    private $serviceUrl;
    /**appkey*/
    private $appkey;
    /**secret*/
    private $appSecret;


    /**默认加密方式:不输入使用sha256,其它可选择项md5,sha1,sha3-256*/
    private $encryMethod;
    /**默认ts单位:1毫秒,2秒*/
    private $tsType;
    /**
     * RequestUtils constructor.
     * @param $serviceUrl
     * @param $appkey
     * @param $appSecret
     */
    public function __construct($serviceUrl, $appkey, $appSecret,$encryMethod=null,$tsType=2){
        Assert::notBlank($serviceUrl,"serviceUrl不能为空");
        Assert::notBlank($appkey,"appkey不能为空");
        Assert::notBlank($appSecret,"appSecret不能为空");
        $this->serviceUrl = $serviceUrl;
        $this->appkey = $appkey;
        $this->appSecret = $appSecret;
        $this->encryMethod = $encryMethod;
        $this->tsType = $tsType;
        if(!is_null($this->encryMethod)){
            $this->encryMethod=strtolower($this->encryMethod);
        }
    }

    /**
     * @param $path
     * @return object
     */
    public function doPost($path,$req=null){
        Assert::notBlank($path,"path不能为空");
        $url=$this->serviceUrl.$path;
        if($req==null){
            $req=Array();
        }else if(is_array($req)){
            //
        }else if(is_a($req,"com\junziqian\sdk\bean\Req2MapInterface")){
            $req=$req->build();
        }else{
            throw new ResultInfoException("不支持的请求req");
        }
        $req=$this->fillSign($req);
        //请求服务端sass
        //print_r(CommonUtil::json_encode($req));
        $response= HttpClientUtils::getPost($url,$req);
        $res=json_decode($response);
        Assert::notNull($res,"不能转换为JSON:".$response);
        return $res;
    }

    /**
     * 填充签名数据
     * @param $req array
     */
    public function fillSign($req){
        /**默认加密方式:不输入使用sha256,其它可选择项md5,sha1,sha3-256*/
        $ts=time();
        if($this->tsType==1){
          $ts=$ts*1000;
        }
        $sign=null;
        $nonce= md5($ts."");
        $signSrc="nonce".$nonce."ts".$ts."app_key".$this->appkey."app_secret".$this->appSecret;
        if($this->encryMethod==null||$this->encryMethod=="sha256"){
            $sign=ShaUtils::getSha256($signSrc);
        }else if($this->encryMethod=="sha1"){
            $sign=ShaUtils::getSha1($signSrc);
        }else if($this->encryMethod=="md5"){
            $sign=md5($signSrc);
        }else{
            throw new ResultInfoException($this->encryMethod.",必须为md5,sha1,sha256之一","PARAM_ERROR");
        }
        $req['ts']=$ts;
        $req['app_key']=$this->appkey;
        $req['sign']=$sign;
        $req['nonce']=$nonce;//这只是为了生成一个随机值
        if($this->encryMethod!=null){
            $req['encry_method']=$this->encryMethod;//为''也不能传
        }
        return $req;
    }
}