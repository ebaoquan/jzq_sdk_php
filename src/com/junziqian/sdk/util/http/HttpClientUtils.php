<?php
namespace com\junziqian\sdk\util\http;

use com\junziqian\sdk\util\exception\ResultInfoException;

/**
 * Class HttpClientUtils
 * @package com\junziqian\sdk\util\http
 * @edit yfx 2019-10-29
 */
class HttpClientUtils{
    /**
     * post请求
     * @param $url string
     * @param $req array
     * @return string
     */
    public static function getPost($url,$req){
        //ini_set('user_agent','Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; GreenBrowser)');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //验证curl对等证书(一般只要此项)
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //检查服务器SSL证书中是否存在一个公用名
        curl_setopt($ch, CURLOPT_SSLVERSION, 0);  //传递一个包含SSL版本的长参数。
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        if(version_compare(PHP_VERSION, '5.6')&&!version_compare(PHP_VERSION, '7.0')){
            curl_setopt ( $ch, CURLOPT_SAFE_UPLOAD, false);
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        $response = curl_exec($ch);
        if(!curl_error($ch)) {
            return $response;
        } else {
            throw new ResultInfoException(curl_error($ch),"POST_ERROR");
        }
    }

    /**
     * http文件下载
     * @param $url 原文件地址
     * @param string $file 文件路径
     * @param int $timeout 超时设置,默认60秒
     * @return bool|mixed|string
     */
    public static function httpcopy($url, $file="", $timeout=60) {
        $file = empty($file) ? pathinfo($url,PATHINFO_BASENAME) : $file;
        $dir = pathinfo($file,PATHINFO_DIRNAME);
        !is_dir($dir) && @mkdir($dir,0755,true);
        $url = str_replace(" ","%20",$url);
        if(function_exists('curl_init')) {

            $headers['User-Agent'] = 'windows';
            $headerArr = array();
            foreach( $headers as $n => $v ) {
                $headerArr[] = $n .':' . $v;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArr);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $temp = curl_exec($ch);
            if(@file_put_contents($file, $temp) && !curl_error($ch)) {
                return $file;
            } else {
                throw new ResultInfoException(curl_error($ch),"POST_ERROR");
            }
        } else {
            $params = array(
                "http"=>array(
                    "method"=>"GET",
                    "header"=>"User-Agent:windows",
                    "timeout"=>$timeout)
            );
            $context = stream_context_create($params);
            if(@copy($url, $file, $context)) {
                //$http_response_header
                return $file;
            } else {
                return false;
            }
        }
    }
}