<?php

namespace com\junziqian\sdk\util;

use com\junziqian\sdk\util\exception\ResultInfoException;

/**
 * Class Assert 断言工具，方便抛出异常，由统一异常处理工具类捕获异常直接返回
 * @package com\junziqian\sdk\util
 */
class Assert{
    /**
     * 判断为真
     * @param $flag bool 判断结果
     * @param $msg string 为空时异常信息
     */
    static function isTrue($flag,$msg="值不为True"){
        if(!$flag){
            throw new ResultInfoException($msg,"PARAM_ERROR");
        }
    }

    /**
     * 断言为NULL
     * @param $flag
     * @param string $msg
     */
    static function isNull($flag,$msg="值不为NULL"){
        if(!is_null($flag)){
            throw new ResultInfoException($msg,"PARAM_ERROR");
        }
    }

    /**
     * 断言不为空
     * @param $flag
     * @param string $msg
     */
    static function notNull($flag,$msg="值为NULL"){
        if(is_null($flag)){
            throw new ResultInfoException($msg,"PARAM_ERROR");
        }
    }

    /**
     * 断方不为BLANK
     * @param $flag
     * @param string $msg
     */
    static function notBlank($flag,$msg="值为BLANK"){
        if(is_null($flag)||trim($flag)==''||$flag=='null'){
            throw new ResultInfoException($msg,"PARAM_ERROR");
        }
    }
}