<?php
namespace com\junziqian\sdk\util;

/**
 * Class CommonUtil 通用工具类
 * @package com\junziqian\sdk
 */
class CommonUtil{

    /**
     * 使json_encode支持5.4.0以下
     * @param $value object|array 传入为对象
     * @return mixed|string
     */
    static function json_encode($value){
        if (version_compare(PHP_VERSION,'5.4.0','<')){
            $str = json_encode($value);
            $str = preg_replace_callback("#\\\u([0-9a-f]{4})#i",
                function($matchs){
                    return iconv('UCS-2BE', 'UTF-8', pack('H4', $matchs[1]));
                },
                $str
            );
            return $str;
        }else{
            return json_encode($value, JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * 处理无效params转数字null或0为'0'
     * @param null $str
     * @return string
     */
    static function trim($str=null){
        if(is_null($str)){
            if(is_numeric($str)){
                return '0';
            }
            return '';
        }else{
            return trim($str.'');
        }
    }
}