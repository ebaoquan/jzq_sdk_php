<?php
namespace com\junziqian\sdk\bean;
use com\junziqian\sdk\util\CommonUtil;

/**
 * Interface Req2MapInterface
 * @package com\junziqian\sdk\bean
 * @edit yfx 2019-10-29
 */
abstract class Req2MapInterface{
    /**
     * @return mixed object转array(第二层要看情况转，所以无法通用)
     */
    public function build(){
        $arr= self::getObject2Array($this);
        return $arr;
    }


    /**
     * 对象转为数组,如果已经是数组，则会清空数组
     * @param $obj
     * @param $immPramas array 直转不做处理的属性
     * @return array
     */
    static function getObject2Array($obj,$ignoreParams=null){
        $_array = is_object($obj) ? get_object_vars($obj) : $obj;
        $array=array();
        foreach ($_array as $key => $value) {
            if(is_null($value)){
                continue;
            }else if($ignoreParams!=null&&in_array($key, $ignoreParams)){
                continue;
            }else if(is_a($value,'CURLFile')){
                $array[$key] = $value;//文件直接处理
            }else if(is_array($value)){
                $array[$key] = CommonUtil::json_encode($value);//文件直接处理
            }else if(is_a($value,"com\junziqian\sdk\bean\Req2MapInterface")){
                $array[$key] = CommonUtil::json_encode($value);//
            }else if(is_object($value)){
                //is_object 对数字、数组等都是返回false
                $array[$key] = CommonUtil::json_encode($value);//
            }else{
                if(is_string($value)&&""==$value){
                    continue;
                }
                $array[$key] = $value;//文件直接处理
            }
        }
        return $array;
    }
}