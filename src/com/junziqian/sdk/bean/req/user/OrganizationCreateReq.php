<?php
namespace com\junziqian\sdk\bean\req\user;

use com\junziqian\sdk\bean\Req2MapInterface;

/**
 * Class OrganizationCreateReq 组织创建及重传
 * @package com\junziqian\sdk\bean\req\user
 * @edit yfx 2019-10-29
 */
class OrganizationCreateReq extends Req2MapInterface{

    //@ApiModelProperty(value = " 邮箱或手机号",required = true)
    public $emailOrMobile;

    //@ApiModelProperty(value = " 名称",required = true)
    public $name;

    //@ApiModelProperty(value = "组织类型 0企业,1事业单位",required = true,allowableValues = "0,1")
    public $organizationType;

    //@ApiModelProperty(value = "证明类型：0多证,1多证合一",required = true,allowableValues = "0,1")
    public $identificationType;

    //@ApiModelProperty(value = "组织注册编号，营业执照号或事业单位事证号或统一社会信用代码",required = true)
    public $organizationRegNo;

    //@ApiModelProperty(value = "组织注册证件扫描件，营业执照或事业单位法人证书",required = true)
    public $organizationRegImg;

    //@ApiModelProperty(value = "法人姓名",required = false)
    public $legalName;

    //@ApiModelProperty(value = "法人身份证号",required = false)
    public $legalIdentityCard;

    //@ApiModelProperty(value = "法人电话号码",required = false)
    public $legalMobile;

    //@ApiModelProperty(value = "法人身份证正面",required = false)
    public $legalIdentityFrontImg;

    //@ApiModelProperty(value = "法人身份证反面",required = false)
    public $legalIdentityBackImg;

    //@ApiModelProperty(value = "公章签章图片",required = false)
    public $signImg;

    //@ApiModelProperty(value = "法人住址",required = false)
    public $address;

    //@ApiModelProperty(value = "企业授权人姓名",required = false)
    public $authorizeName;

    //@ApiModelProperty(value = "企业授权人身份证号",required = false)
    public $authorizeCard;

    //@ApiModelProperty(value = "企业授权人手机号",required = false)
    public $authorizeMobilePhone;

    //@ApiModelProperty(value = "组织结构代码",required = false)
    public $organizationCode;

    //@ApiModelProperty(value = "组织结构代码扫描件",required = false)
    public $organizationCodeImg;

    //@ApiModelProperty(value = "税务登记扫描件,事业单位选填，普通企业必选",required = false)
    public $taxCertificateImg;

    //@ApiModelProperty(value = "签约申请书扫描图",required = false)
    public $signApplication;
}