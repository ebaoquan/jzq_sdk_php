<?php
namespace com\junziqian\sdk\bean\req\sign\ext;

/**
 * Class SequenceInfo 合同顺序信息
 * @package com\junziqian\sdk\bean\req\sign\ext
 * @edit yfx 2019-10-29
 */
class SequenceInfo{
    //@ApiModelProperty(value = "客户方合同的唯一编号",required = true)
    public $businessNo;

    //@ApiModelProperty(value = "签约顺序号",required = true)
    public $sequenceOrder;

    //@ApiModelProperty(value = "总份数",required = true)
    public $totalNum;

    /**
     * SequenceInfo constructor.
     * @param $businessNo
     * @param $sequenceOrder
     * @param $totalNum
     */
    public function __construct($businessNo, $sequenceOrder, $totalNum){
        $this->businessNo = $businessNo;
        $this->sequenceOrder = $sequenceOrder;
        $this->totalNum = $totalNum;
    }

}