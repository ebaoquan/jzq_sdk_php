<?php
namespace com\junziqian\sdk\bean\req\sign\ext;

/**
 * Class SignatoryReq 签约方信息
 * @package com\junziqian\sdk\bean\req\sign\ext
 * @edit yfx 2019-10-29
 */
class SignatoryReq{
    //@ApiModelProperty(value = "签约方名称,不超过50个字符",required = true)
    public $fullName;

    //@ApiModelProperty(value = "身份类型:1身份证,2护照,3台胞证,4港澳居民来往内地通行证,11营业执照,12统一社会信用代码",required = true)
    public $identityType;

    //@ApiModelProperty(value = "证件号，不超过50个字符",required = true)
    public $identityCard;

    //@ApiModelProperty(value = "手机号码（个人必传），11个字符")
    public $mobile;

    //@ApiModelProperty(value = "邮箱,企业必传")
    public $email;

    //@ApiModelProperty(value = "签字顺序:连续签（orderNum只是针对于当前合同）,顺序签时需指定")
    public $orderNum;

    //@ApiModelProperty(value = "签字位置座标信息:positionType=0时可以传入chapteJson")
    public $chapteJson;

    //@ApiModelProperty(value = "签字位置-表单域名ID:positionType=1时必须传入")
    public $chapteName;

    //@ApiModelProperty(value = "签字位置-按关键字签署，positionType=2时必须传入，关键字支持多个;以英文;分隔")
    public $searchKey;

    //@ApiModelProperty(value = "签字位置-按关键字查询-扩展，positionType=2时可以传入,支持指定查询页数/关键字颜色/透明度.可参考相关说明")
    public $searchExtend;

    //@ApiModelProperty(value = "签字位置-按关键字查询-结果转换的配置，positionType=2时可以传入,可配置查询结果的位置偏移.可参考后面说明")
    public $searchConvertExtend;

    //@ApiModelProperty(value = "签约方需要手签时是否不先行验证手机或邮箱:1不验证，其它验证(默认)")
    public $noNeedVerify;

    //@ApiModelProperty(value = " 是否使用自动签，0或null不使用，1自动(当且只当合同处理方式为部份自动或收集批量签时有效);有些场景必须serverCaAuto=1")
    public $serverCaAuto;

    //@ApiModelProperty(value = "验证等级(传数组字符串):[2,3];2银行卡认证,10三要素认证,11人脸识别,12验证码验证")
    public $authLevel;

    //@ApiModelProperty(value = "最小验证种类:默认为authLevel数组长度;必须小于authLevel长度且大于0(等于0时按authLevel数组长度计录);如authLevel=[2,3,10],authLevelRange=2表过只需要验证其中的两项即可")
    public $authLevelRange;

    //@ApiModelProperty(value = "签字类型，标准图形章或公章:0标准图形章,1公章或手写,2公章手写或手写")
    public $signLevel;

    //@ApiModelProperty(value = "强制添加现场:0或null：不强制添加现场,1：强制添加现场")
    public $forceEvidence;

    //@ApiModelProperty(value = "买保险年数:1-3购买年限,基它不买;注需要有相应的套餐")
    public $insureYear;

    //@ApiModelProperty(value = "强制阅读多少秒，单位1-300秒")
    public $readTime;

    //@ApiModelProperty(value = "企业用户指定签章ID:此值需为商户上传的自定义公章ID，或商户创建的企业的自定义公章ID。自定义公章可通过sass或api上传")
    public $signId;

    //@ApiModelProperty(value = "标准章时是否对个人或企业章图片打码：0不打，1打码")
    public $nameHideStatus;

    //@ApiModelProperty(value = "h5人脸订单号,如使用过君子签提供的人脸认证服务可以上传其订单号")
    public $h5FaceOrderNo;

    //@ApiModelProperty(value = "现场存证只能上传视频:1是，其它不是(默认)")
    public $onlyVideoEvidence;

    //@ApiModelProperty(value = "现场存证自定义标题")
    public $evidenceTitle;

    //@ApiModelProperty(value = "是否使用电子保管函：1使用0或其它不使用;使用时需有相应套餐")
    public $safeKeepLetterFlag;

    //@ApiModelProperty(value = "api发起显示确认签字")
    public $apiAffirm;

}