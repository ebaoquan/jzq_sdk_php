<?php
namespace com\junziqian\sdk\bean\req\sign;

use com\junziqian\sdk\bean\Req2MapInterface;
use com\junziqian\sdk\util\CommonUtil;

/**
 * Class ApplySignReq 签约发起-合同
 * @package com\junziqian\sdk\bean\req\sign
 * @edit yfx 2019-10-29
 */
class ApplySignReq extends Req2MapInterface {

    //@ApiModelProperty(value = "合同名称",required = true)
    public $contractName;

    //@ApiModelProperty(value = "签收方",required = true)
    public $signatories;

    //@ApiModelProperty(value = "是否需要服务端证书，云证书：非1不需要，默认;1需要")
    public $serverCa;
    /**处理方式*/
    //@ApiModelProperty(value = "处理方式:为空或0时默认为手签合同;1自动签约;2只保全;5部份自动签;6HASH只保全;7收集批量签")
    public $dealType;

    //@ApiModelProperty(value = "dealType=6时必须传入,文件的sha512HexString值")
    public $hashValue;

    /**-----------合同文件相关----------**/
    //@ApiModelProperty(value = "合同上传方式:0或null直接上传PDF;1url地址下载;2tmpl模版生成;3html文件上传")
    public $fileType;

    //@ApiModelProperty(value = "dealType!=6,fileType=0或null,时必须传入,合同文件;请使用form表单上传文件")
    public $file;

    //@ApiModelProperty(value = "dealType!=6,fileType=1,时必须传入,合同PDF文件的url地址")
    public $url;

    //@ApiModelProperty(value = "dealType!=6,fileType=2,时必须传入,合同模版编号")
    public $templateNo;

    //@ApiModelProperty(value = "dealType!=6,fileType=2,时必须传入,合同模版参数JSON字符串")
    public $templateParams;

    //@ApiModelProperty(value = "dealType!=6,fileType=3,时必须传入,合同html文件")
    public $htmlContent;
    /**-----------合同文件相关 end----------**/

    //@ApiModelProperty(value = "指定公章位置类型:0或null使用签字座标位置或不指定签字位置;1表单域定位(表单域如果上传为pdf时,需pdf自行定义好表单域,html及url及tmpl等需定义好input标签);2关键字定义")
    public $positionType;

    //@ApiModelProperty(value = "验证方式为人脸时必传,人脸识别等级:默认等级(1-100之间整数)，建议范围(60-79)")
    public $faceThreshold;

    //@ApiModelProperty(value = "是否按顺序签字，非1为不按，1为按")
    public $orderFlag;

    //@ApiModelProperty(value = "合同查看二维码0默认不1显示")
    public $qrCode;

    //@ApiModelProperty(value = "不显示ebq的保全章:1 不显示但会签名,2不显示也不签名;0或其它-显示")
    public $noEbqSign;

    //@ApiModelProperty(value = "合同金额")
    public $contractAmount;

    //@ApiModelProperty(value = "备注")
    public $remark;

    //@ApiModelProperty(value = " 前置记录，此记录会计录到签约日志中，并保全到日志保全和最终的证据保全中，最大字符不能超过2000字符串")
    public $preRecored;

    //@ApiModelProperty(value = "多合同顺序签约Info")
    public $sequenceInfo;

    //@ApiModelProperty(value = "合同附件,虽不限个数,但包括合同原文件,不能超过20MB")
    public $attachFiles;

    //@ApiModelProperty(value = "是否使用视频签约:0或null不使用;1使用(使用时必须购买相应套餐)")
    public $ifWebRtc;

    //@ApiModelProperty(value = "是否使用骑缝章:1使用;其它不使用")
    public $needQifengSign;

    //@ApiModelProperty(value = "是否归档:0不归档;1归档(默认)")
    public $isArchive;

    //@ApiModelProperty(value = "是否可以拒签:0或null不能拒签(默认);1可拒签")
    public $canRefuse;

    //@ApiModelProperty(value = "是否不显示个人标准章边框:1不显示,其它显示边框(默认)")
    public $noBorderSign;

    //@Override
    public function build() {
        $arr= self::getObject2Array($this,array("attachFiles"));
        if($this->attachFiles!=null&&sizeof($this->attachFiles)>0){
            $i=0;
            foreach($this->attachFiles as $value){
                $arr["attachFiles[".$i."]"]=$value;
                $i = $i+1;
            }
        }
        return $arr;
    }

}