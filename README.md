# jzq_sdk_php
#一、环境依赖  

##php版本
> php最低版本以composer.json中require.php为准，现为>=5.3.39

## 以前版本
> 此程序是对ebq/jzq_api工具类在生产交互使用过程，存在的校验难、验证难等问题的升级解决方案。  
> 重构了君子签的SDK和API服务体系。使用了更简单的签名过程和请求过程，但不失安全性。  
> php的sdk签名方式支持md5(32位),sha1,sha256等方案

#二、安装方式
##1.使用composer管理工具安装
> 首先：composer.json的require中加入"ebaoquan/junziqian_sdk":">=2.0.0"(版本以composer.json中version为准) 

> 然后执行composer update即可(或 php composer.phar update) 
```
> composer update
```
##2.没有使用composer工具 
> 请联系对接人员，获取完整包 

#三、使用 
##1.开始
```
1.参考example例子目录，修改例子中的$serviceUrl,$appkey,$appSecret 变量
2.对部份信息填值，然后执行example例子.php即可 
3.不使用example，首先php头：require_once \__DIR\__ . '/../../vendor/autoload.php';这个autoload.php文件(本身项目已使用composer管理则不需要)，其它工具使用时请use引用
```
##2.请求
###ping服务
```php
<?php
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="xxxxxxxx";
$appSecret="xxxxxxxxxxxxxxx";
//发起PING请求
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);
$response=$requestUtils->doPost("/v2/ping");
//处理结果
print_r( CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));
```
###使用array直接请求
> array请求用于参数简单的情况
```
//初始化合同参数
$request=array(
    "name" => "易XX",
    "identityCard" => "500240XXXXXXXXXX"
);
//发起请求
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);
$response=$requestUtils->doPost("/v2/auth/userValid",$request);
```

###使用对象请求
> 对象请求用于参数复杂的情况
```
$request=new ApplySignReq();//必须extends至Req2MapInterface类
$request->contractName="合同名称";
$response=$requestUtils->doPost("/v2/sign/applySign",$request);
```
##3.指定签名方式（即生成sign方式,php默认为sha256）
```php
$encryMethod="md5";//md5,sha1,sha256(默认)
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret,$encryMethod);
```

##4.指定时间戳单位（即生成ts方式,php默认为秒为单位）
```php
$tsType=1;//1毫秒,2.秒(默认)
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret,null,$tsType);
```

##php5.3-php5.5之间的版本说明
> php5.3与php5.5 不要使用CURLFile，因为这个函数还不存在,而是使用: '@D:/tmp/test.png'方式。