<?php

use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

/**
 * 人脸认证
 * User: huhu
 * @edit yfx 2019-10-29
 */
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="xxxxxxxx";
$appSecret="xxxxxxxxxxxxxxx";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);
//初始化合同参数
$request=array(
    "orderNo" => "XXXXXXXXXXX",
    "name" => "易XX",
    "identityCard" => "50024019XXXXXXXXXX",
    "backUrl" =>"http://xxx.xx.xx"//启动方式1=browser ：表示在浏览器启动刷2=app ：表示在 app 里启动刷脸 默认值为browser
    //"startFrom" => 1
);

//发起请求
$response=$requestUtils->doPost("/v2/auth/startH5Face",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));