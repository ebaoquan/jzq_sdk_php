<?php

/**
 * OCR身份证识别
 * User: huhu
 * @edit yfx 2019-10-29
 */
//引入composer的入口函数,参考composer的使用规范
use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

require_once __DIR__ . '/../../../../vendor/autoload.php';

//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="xxxxxxxx";
$appSecret="xxxxxxxxxxxxxxx";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);
//初始化合同参数
$request=array(
    "file" => new CURLFile('D:/tmp/iden.png',null,"test.png"),
    "isCompress" => 0 //0压缩1不压缩，默认0
    //"imgHttpUrl" => "http://xxx.xxx.xxx"
);
//发起请求
$response=$requestUtils->doPost("/v2/ocr/ocrIdentity",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));
//{"data":{"address":"重庆市XXXXXXX193号","birthday":"19XXXXX","executeStatus":0,"gender":"男","idNo":"500240XXXXXXXXXX","name":"易XX","nation":"土家","orderNo":"E5E11393244XXXXXXX","resultMessage":"success"},"success":true}