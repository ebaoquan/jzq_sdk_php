<?php

/**
 * 下载企业自定义公章
 * User: huhu
 * @edit yfx 2019-10-30
 */
//引入composer的入口函数,参考composer的使用规范
use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

require_once __DIR__ . '/../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="xxxxxxxx";
$appSecret="xxxxxxxxxxxxxxx";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);

//初始化合同参数
$request=array(
    "email" => "500XXXXXXXXXXXX", //TODO * 企业注册的邮箱
    "signId" => 1, //TODO * 公章ID
);

//发起请求
$response=$requestUtils->doPost("/v2/user/entSignDown",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));
file_put_contents("/test001.png", base64_decode($response->data->entSignDate));//保存到本地
printf("保存成功");