<?php
use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

/**
 * 企业自定义公章上传
 * User: huhu
 * @edit yfx 2019-10-30
 */
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="xxxxxxxx";
$appSecret="xxxxxxxxxxxxxxx";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);

//初始化合同参数
$request=array(
    "signName" => "500XXXXXXXXXXXX", //TODO *
    "email" => "500XXXXXXXXXXXX", //TODO 不传则保存在商户下，传入注册的邮箱则上传到指定邮箱企业名下
    "signImgFile" => new CURLFile('D:/tmp/test.png',null,"test.png"),
);
$response=$requestUtils->doPost("/v2/user/uploadEntSign",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));