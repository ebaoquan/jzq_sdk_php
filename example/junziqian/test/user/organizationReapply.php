<?php

use com\junziqian\sdk\bean\req\user\OrganizationCreateReq;
use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

/**
 * 企业信息重传
 * User: huhu
 * @edit yfx 2019-10-30
 */
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="7806bebXXXXXXXXXXXXXX";
$appSecret="7f34876d7806bXXXXXXXXXXXX";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);
//CURLFile 可以传url或filePath，但必须保证文件存在且有效，否则php不会报错，只会导致http请求返回null（并没有调到服务端）。
//初始化合同参数
$request=new OrganizationCreateReq();
$request->emailOrMobile="xxx@ebq.org";//TODO *
$request->name="XXXXXXXXXX发有限责任公司";
$request->identificationType=1;
$request->organizationType=0;
$request->organizationRegNo="91620XXXXXXXXX5XG";
$request->organizationRegImg= new CURLFile('D:/tmp/test.png',null,"test.png");
$request->legalName="易XX";//法人
$request->legalIdentityCard="5002401XXXXXXXXX";//法人证件号
$request->legalMobile="153XXXXXXXX";

//发起创建企业请求
$response=$requestUtils->doPost("/v2/user/organizationReapply",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));