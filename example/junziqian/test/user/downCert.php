<?php

use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

/**
 * 证书申请补传资料
 * User: huhu
 * @edit yfx 2019-10-30
 */
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="xxxxxxxx";
$appSecret="xxxxxxxxxxxxxxx";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);

//初始化合同参数
$request=array(
    "identityCard" => "500XXXXXXXXXXXX", //TODO * 证件号
    "identityType" => 1, //TODO * 证件类型 1身份证, 2护照, 3台胞证, 4港澳居民来往内地通行证, 11营业执照, 12统一社会信用代码
    "name" => "易XX", //TODO * 证件名称
    //"email" => "500XXXXXXXXXXXX", //TODO * 企业注册时的邮箱账号,当为企业时必填
);

//发起请求
$response=$requestUtils->doPost("/v2/user/downCert",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));