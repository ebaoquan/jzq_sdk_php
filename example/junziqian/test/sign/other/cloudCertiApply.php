<?php

use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

/**
 * 云证书不落地签署:
 * 将pdf的签署与证书分离，需本地有对签名后的hash写入pdf的能力
 * User: huhu
 * @edit yfx 2019-10-30
 */
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="xxxxxxxx";
$appSecret="xxxxxxxxxxxxxxx";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);

//初始化合同参数
$request=array(
    "fullName" => "易XX", //TODO *
    "identityCard" => "5002XXXXXXXXXXXXXXXXXX", //TODO *
    "identityType" => 1, //TODO *
    "mobile" => "153XXXXXXX", //TODO *
    "email" => "XXXX@XXX.com", //TODO *
    "hashValue" => "xxxxx" //要签署的hash
);
$response=$requestUtils->doPost("/v2/sign/cloudCertiApply",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));