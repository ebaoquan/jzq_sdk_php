<?php
use com\junziqian\sdk\bean\req\sign\ApplySignReq;
use com\junziqian\sdk\bean\req\sign\ext\SignatoryReq;
use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;
use CURLFile;

/**
 * User: huhu
 * @edit yfx 2019-10-29
 */
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="7806bebXXXXXXXXXXXXXX";
$appSecret="7f34876d7806bXXXXXXXXXXXX";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);
//CURLFile 可以传url或filePath，但必须保证文件存在且有效，否则php不会报错，只会导致http请求返回null（并没有调到服务端）。
//初始化合同参数
$request=new ApplySignReq();
$request->contractName="合同名称";
$request->serverCa=1;//自动签
$request->file= new CURLFile('D:/tmp/test.pdf',null,"test1.pdf");
/**
$request->attachFiles= array(
    new CURLFile('D:/tmp/test.jpg',null,"test1.jpg"),
    new CURLFile('D:/tmp/test.png',null,"test1.png"));*///需保证文件存在
$sReq=new SignatoryReq();
$sReq->fullName="易XX";
$sReq->identityType=1;
$sReq->identityCard="50024XXXXXXXXXXXXXXXX";
$sReq->mobile="153XXXXXXXXXX";
$sReq->orderNum=1;
$sReq->chapteJson="[{\"page\":0,\"chaptes\":[{\"offsetX\":0.12,\"offsetY\":0.23}]},{\"page\":1,\"chaptes\":[{\"offsetX\":0.45,\"offsetY\":0.67}]}]";
$sReq->noNeedVerify=1;
$sReq->serverCaAuto=1;
$request->signatories=array($sReq);

//发起PING请求
$response=$requestUtils->doPost("/v2/sign/applySign",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));