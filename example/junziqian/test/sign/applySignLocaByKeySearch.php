<?php

use com\junziqian\sdk\bean\req\sign\ApplySignReq;
use com\junziqian\sdk\bean\req\sign\ext\SignatoryReq;
use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

/**
 * 使用文件发起合同
 * User: huhu
 * @edit yfx 2019-10-29
 */
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="7806bebXXXXXXXXXXXXXX";
$appSecret="7f34876d7806bXXXXXXXXXXXX";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);

//CURLFile 可以传url或filePath，但必须保证文件存在且有效，否则php不会报错，只会返回null。
//初始化合同参数
$request=new ApplySignReq();
$request->contractName="合同名称";
$request->serverCa=1;//使用云证书
$request->file = new CURLFile('D:/tmp/test.pdf',null,"test1.pdf");
/**
$request->attachFiles= array(
new CURLFile('D:/tmp/test.jpg',null,"test1.jpg"),
new CURLFile('D:/tmp/test.png',null,"test1.png"));*/
$sReq=new SignatoryReq();
$sReq->fullName="易凡翔";
$sReq->identityType=1;
$sReq->identityCard="500240198704146355";
$sReq->mobile="15320369150";
$sReq->orderNum=1;
$sReq->searchKey="ebq";//搜索关键字
//$sReq->searchExtend=array( //搜索关键字扩展,可以是json_string或array
//    "page"=>0, //支持数字(支持负数)和数组,也可以是"page" => array(1,2,3...) 页码以0为首页,支持页码为负数方式（例-1为最后一页）
//    "fillColor" => array( //字体颜色过滤
//        "red" => 0,   //红0-255，默认0
//        "green" => 0, //绿0-255，默认0
//        "blue" => 0,  //蓝0-255，默认0
//        "alpha" => 255 //透明度0-255，默认255
//    )
//);
//$sReq->searchConvertExtend=array( //关键字结果位置偏移设定,可以是json_string或array
//        "fixX" =>-20.0, //签字位置x偏移
//        "fixY" =>-20.0  //签字位置y偏移
//    )
$request->signatories=array($sReq);

//发起PING请求
$response=$requestUtils->doPost("/v2/sign/applySign",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));