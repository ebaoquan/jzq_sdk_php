<?php

use com\junziqian\sdk\bean\req\sign\ApplySignReq;
use com\junziqian\sdk\bean\req\sign\ext\SignatoryReq;
use com\junziqian\sdk\util\Assert;
use com\junziqian\sdk\util\CommonUtil;
use com\junziqian\sdk\util\RequestUtils;

/**
 * 使用html单文件发起合同
 * User: huhu
 * @edit yfx 2019-10-29
 */
//引入composer的入口函数,参考composer的使用规范
require_once __DIR__ . '/../../../../vendor/autoload.php';
//定义初始化变量，这些变量在生产中请设为全局变量或通用变量
$serviceUrl="http://sandbox.api.junziqian.com";
$appkey="7806bebXXXXXXXXXXXXXX";
$appSecret="7f34876d7806bXXXXXXXXXXXX";
//构建请求工具
$requestUtils=new RequestUtils($serviceUrl, $appkey, $appSecret);

//CURLFile 可以传url或filePath，但必须保证文件存在且有效，否则php不会报错，只会返回null。
//初始化合同参数
$request=new ApplySignReq();
$request->contractName="合同名称";
$request->serverCa=1;//使用云证书
$request->fileType=3;
$request->positionType=1;//指定公章位置类型:0或null使用签字座标位置或不指定签字位置;1表单域定位(表单域如果上传为pdf时,需pdf自行定义好表单域,html及url及tmpl等需定义好input标签);2关键字定义
$request->htmlContent = "<meta charset=\"utf-8\">html文件信息001<br/><br/><br/><br/><br/><br/><br/><br/><input type=\"text\" name=\"ebq\" style=\"width:0;height:0;border:0;margin:0;padding:0;position: relative;\">".
    "<br/><br/><br/><br/><br/><br/><br/><br/><input type=\"text\" name=\"ebq\" style=\"width:0;height:0;border:0;margin:0;padding:0;position: relative;\">";
$sReq=new SignatoryReq();
$sReq->fullName="易凡翔";
$sReq->identityType=1;
$sReq->identityCard="500240198704146355";
$sReq->mobile="15320369150";
$sReq->chapteName="ebq";//使用表单域定位,对上传fileType是html和模版的可以直接配置；如果是pdf，需要此pdf为一个有表单域的pdf
$request->signatories=array($sReq);

//发起PING请求
$response=$requestUtils->doPost("/v2/sign/applySign",$request);
//处理结果
print_r(CommonUtil::json_encode($response));
Assert::notNull($response,"返回的结果不能为空");
Assert::isTrue($response->success,"处理错误:".CommonUtil::json_encode($response));
//{"data":"APL1189435886153502720","success":true}